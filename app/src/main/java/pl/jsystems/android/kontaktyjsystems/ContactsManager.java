package pl.jsystems.android.kontaktyjsystems;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

public class ContactsManager {
    private static final String[] CONTACT_PROJECTION = {
            ContactsContract.CommonDataKinds.Phone._ID,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID
    };
    private ContentResolver contentResolver;

    public ContactsManager(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    public List<Contact> getContacts() {
        String query = "604";
//        String query = "Marcin";
        Cursor cursor = contentResolver.query(
                Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI, query),
                CONTACT_PROJECTION,
                null,
                null, null);

        List<Contact> result = new ArrayList<>();
        // Przy Cursor, zawsze trzeba zawolac moveToNext() przed pobraniem pierwszego wiersza
        while (cursor.moveToNext()) {
            Contact contact = new Contact();
            contact.setId(cursor.getInt(0));
            contact.setName(cursor.getString(1));
            contact.setPhoneNumber(cursor.getString(2));
            contact.setEmail(findEmail(cursor.getInt(3)));

            result.add(contact);
        }

        cursor.close();
        return result;
    }

    private String findEmail(int contactId) {
        Cursor cursor = contentResolver.query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Email.ADDRESS},
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{String.valueOf(contactId)},
                null);

        if (cursor.getCount() > 0) {
            cursor.moveToNext();
            String email = cursor.getString(0);
            cursor.close();
            return email;
        }

        cursor.close();
        return null;
    }
}
